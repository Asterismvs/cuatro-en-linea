

    class Meths
        
        def initialize(columns, board, nTurno, players, lastPlayer)
                  @columns = columns
                  @board = board
                  @nTurno = nTurno
                  @players = players
                  @lastPlayer = lastPlayer
                end
                
                
                
        #Metodo referi, decide quien gana
        def referiX1(board, seña)
            return false if !@board.each_with_index {|row, iB| 
                row.chars.each_with_index {|casilla, iR|
                    if @board[iB][iR] == seña && @board[iB][iR+2] == seña && @board[iB][iR+4] == seña && @board[iB][iR+6] == seña
                        return true
                    elsif @board[iB][iR] == seña && @board[iB-1][iR] == seña && @board[iB-2][iR] == seña && @board[iB-3][iR] == seña
                        return true
                    elsif @board[iB][iR] == seña && @board[iB-1][iR+2] == seña && @board[iB-2][iR+4] == seña && @board[iB-3][iR+6] == seña 
                        return true
                    elsif @board[iB][iR] == seña && @board[iB-1][iR-2] == seña && @board[iB-2][iR-4] == seña && @board[iB-3][iR-6] == seña
                        return true
                    end}}         
                end
        #Metodo de movimiento para jugadores y MaquinIA
        def moves 
            system 'clear'
            puts @board
            seña = @nTurno%2==0 ? 'o' : 'x'
            if @nTurno%2 != 0 
                #turno de jugador
                puts "Es el turno de ".cyan + "#{@players[:name1]}".cyan.underline.bold
                puts "Selecciona una de las 7 columnas".cyan
    
                ficha = gets.to_i
                while ficha < 1 || ficha > 7
                    puts "#{@players[:name1]}... te dije que elijas una columna... del 1 al 7!".red.bold
                    ficha = gets.to_i
                end
                @lastPlayer = @players[:name1]
    
            elsif @nTurno%2 == 0 && @players[:name2] == ''
                #turno de maquina
                ficha = rand(1..7)
                @lastPlayer = 'MaquinIA'
            
    
            else
                #turno de jugador 2
                puts "Es el turno de ".magenta + "#{@players[:name2]}".magenta.underline.bold
                puts "Selecciona una de las 7 columnas".magenta
                ficha = gets.to_i
                while ficha < 1 || ficha > 7
                    puts "#{@players[:name2]}... te dije que elijas una columna... del 1 al 7!".red.bold
                    ficha = gets.to_i
                end
    
                @lastPlayer = @players[:name2]
            
            end
    
            column = @columns[ficha]
                @board.reverse.each_with_index do |row, i|
                    if row[column] == '_' && i < 6
                        row[column] = seña
                    
                        break
                    end
                end
            puts @board
            if referiX1(@board, seña)
                puts "Ganador #{@lastPlayer}".green.bold.blink
                sleep 0.1
                return true
            end
        end
    end

