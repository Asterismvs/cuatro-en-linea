
require( './methods.rb')
   
=begin 
6 filas y 7 columnas
21 fichas cada une
=end
class String
    def red;            "\e[31m#{self}\e[0m" end
    def green;          "\e[32m#{self}\e[0m" end
    def magenta;        "\e[35m#{self}\e[0m" end
    def cyan;           "\e[36m#{self}\e[0m" end
    def bold;           "\e[1m#{self}\e[22m" end
    def underline;      "\e[4m#{self}\e[24m" end
    def blink;          "\e[5m#{self}\e[25m" end
end
#Hashes para indexar cada columna
columns = {
    1=>1,
    2=>3,
    3=>5,
    4=>7,
    5=>9,
    6=>11,
    7=>13
}
#Estilo de strings

  
#Variables
player1 = nil
player2 = nil
maquinIA = nil 
win = false
lastPlayer = ''
nTurno = 1
players = {
    name1:'', 
    name2:''
}

#Variables que conforman el tablero
r_i = '|1|2|3|4|5|6|7|'
r_1 = '|_|_|_|_|_|_|_|'
r_2 = '|_|_|_|_|_|_|_|'
r_3 = '|_|_|_|_|_|_|_|'
r_4 = '|_|_|_|_|_|_|_|'
r_5 = '|_|_|_|_|_|_|_|'
r_6 = '|_|_|_|_|_|_|_|'

#Tablero
board = [r_i,r_1,r_2,r_3,r_4,r_5,r_6]

puts 'Hola, soy la MaquinIA, bienvenido a este "4 en linea"'.red
sleep 0.5
puts 'Puedes elegir jugar contra mí'.red
sleep 0.5 
puts 'O si preferis, podes jugar localmente con quien más quieras'.red
sleep 0.5
puts 'Presiona 1 para jugar conmigo, si no, presiona 2'.red
nPlayers = gets.chomp
sleep 0.2
system 'clear'

if nPlayers == '1'
    puts 'Has elegido jugar contra mí'.red
    sleep 0.5
    puts 'Cual es tu nombre?'.red
    sleep 0.5
    players[:name1] = gets.chomp
    system 'clear'
    puts board

elsif nPlayers == '2'
    puts 'Has elegido jugar contra otra persona'.red
    sleep 0.5
    puts 'Me quedare por aca mirando que nadie haga trampa'.red
    sleep 0.5
    puts '¿Cual es el nombre del player numero 1?'.red
    sleep 0.5
    players[:name1] = gets.chomp
    puts "Y cual es el nombre del contrincante de #{players[:name1]}".red
    sleep 0.5
    players[:name2] = gets.chomp
    puts "Muy bien, #{players[:name2]} jugara con circulos, y #{players[:name1]} jugara con cruces. ¡Empecemos!".red
    sleep 0.5
    puts board   
end  

while  win != true  
    win = Meths.new(columns, board, nTurno, players, player1).moves
    nTurno+=1
end  

