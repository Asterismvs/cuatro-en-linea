# Cuatro en linea

Es un juego de mesa para dos jugadores distribuido por Hasbro, en el que se introducen fichas en un tablero vertical con el objetivo de alinear cuatro consecutivas de un mismo color.

## Reglas del juego

El objetivo es alinear cuatro fichas sobre un tablero formado por seis filas y siete columnas. 

Cada jugador dispone de 21 fichas de un color diferente

Por turnos, los jugadores deben introducir una ficha en la columna que prefieran (siempre que no esté completa) y ésta caerá a la posición más baja. 

Gana la partida el primero que consiga alinear cuatro fichas consecutivas de un mismo color en horizontal, vertical o diagonal

Si todas las columnas están llenas pero nadie ha hecho una fila válida, hay empate.

## El reto

Utilizando Ruby 3, crear una aplicación que se pueda ejecutar en la terminal. 

El juego debe preguntar por el nombre de ambos jugadores e ir intercalando turnos (no es necesario implementar el juego para un@ sol@ jugador@). 

Una vez que algun@ de los dos jugador@s gane se debe detener el juego indicando quien ha ganado.

1. Debes hacer un fork de este repositorio (si no entiendes que es esto, no pasa nada... saber [como funciona git](https://rogerdudler.github.io/git-guide/) es parte del reto) 
2. Programar tu solución
3. Entregar el link del repositorio usando el sistema de careers de aurorajobs.

## Consejos y tips

Te recomendamos que desarrolles el reto tu sol@, si el reto es muy dificil puedes pedir ayuda siempre y cuando entiendas la solución. 

Los retos que encontraras en el día a día seran muy similares a los algoritmos sugeridos en los retos, si aún no estas preparad@ para asumirlos lo mejor es que sigas practicando hasta que te sientas comod@ explicando como funciona el codigo de la aplicación que estas enviando.